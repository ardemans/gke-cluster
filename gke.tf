# Cluster 
# --------
resource "google_container_cluster" "primary" {

  name       = var.clustername
  project    = var.project

  remove_default_node_pool = true
  initial_node_count          = "1"

  addons_config {
    horizontal_pod_autoscaling {
      disabled = "false"
    }

    http_load_balancing {
      disabled = "true"
    }

    network_policy_config {
      disabled = "true"
    }
  }

  cluster_autoscaling {
    enabled = "false"
  }

  # Esto se quita por no poder definirse a la vez aqui y en ip_allocation_policy
  # cluster_ipv4_cidr = "10.252.0.0/20"

  database_encryption {
    state = "DECRYPTED"
  }

  default_max_pods_per_node   = "110"
  enable_binary_authorization = "false"
  enable_kubernetes_alpha     = "false"
  enable_legacy_abac          = "false"
  # enable_shielded_nodes       = "false"

  ip_allocation_policy {
        cluster_secondary_range_name  = var.cluster_secondary_range_name
        services_secondary_range_name = var.services_secondary_range_name
  }

  location        = var.zone
  logging_service = "logging.googleapis.com/kubernetes"

  master_auth {
    client_certificate_config {
      issue_client_certificate = "false"
    }
  }

  monitoring_service = "monitoring.googleapis.com/kubernetes"
  network            = "projects/${var.networkProject}/global/networks/${var.network}"
  subnetwork = "projects/${var.networkProject}/regions/${var.region}/subnetworks/${var.subnetwork}"

  network_policy {
    enabled  = "false"
    provider = "PROVIDER_UNSPECIFIED"
  }

  # node_version = "1.18.16-gke.2100"
  min_master_version = "1.18.16-gke.2100"


  private_cluster_config {
    enable_private_endpoint = "false"
    enable_private_nodes    = "true"
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block
  }

}


# Node pool por defecto (sustitulle al que crea el cluster en si)
# -----------------------------------------------------------------

resource "google_container_node_pool" "primary_default-pool" {
  cluster            = google_container_cluster.primary.name
  # initial_node_count = "1"
  location           = var.zone

  management {
    auto_repair  = "true"
    auto_upgrade = "false"
  }

  
  max_pods_per_node = "110"
  name              = "default-pool"

  node_config {
    disk_size_gb    = "50"
    disk_type       = "pd-standard"
    image_type      = "COS"
    local_ssd_count = "0"
    machine_type    = "n2-standard-2"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes    = [
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
      ]
      
    preemptible     = "true"
    service_account = "default"

    shielded_instance_config {
      enable_integrity_monitoring = "true"
      enable_secure_boot          = "false"
    }

    tags = ["gke-${var.clustername}"]
  }

  # node_count     = "1"
  initial_node_count = "1"
  node_locations = [var.zone]
  project        = var.project

  upgrade_settings {
    max_surge       = "1"
    max_unavailable = "0"
  }

  version = "1.18.16-gke.2100"
}



# Extracción del grupo de instancias de este cluster
# --------------------------------------------------
data "google_compute_instance_group" "gke-primary-default-pool" {
    # self_link = local.gke-test-default-pool-name[0]
    zone = var.zone
    name = regex(".*(gke-${var.clustername}-default-pool-.*-grp)", google_container_node_pool.primary_default-pool.instance_group_urls[0])[0]
}



output "gke_cluster_data" {
  value = google_container_cluster.primary
  sensitive = true
}