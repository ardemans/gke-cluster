
####################################################################################################
# Objetos para balanceo http(s) con ip publica para servir el ingress publico
####################################################################################################



# Named Port
resource "google_compute_instance_group_named_port" "gke-primary-http-ingress-public-port" {
  group = data.google_compute_instance_group.gke-primary-default-pool.self_link
  name = "http-ingress-public"
  port = 9080
}



# Ingress
resource "helm_release" "ingress-public" {
  depends_on        = [google_container_node_pool.primary_default-pool]
  name              = "ingress-nginx-public"
  repository        = "https://kubernetes.github.io/ingress-nginx"
  chart             = "ingress-nginx"
  version           = "3.22.0"
  namespace         = "ingress-nginx-public"
  create_namespace  = "true"
  values            = [file("${path.module}/helm/ingress-nginx-public.yaml")]
}



######
# Check de servidor
####
resource "google_compute_health_check" "gke-primary-public-ingress-check" {
  check_interval_sec = "10"
  healthy_threshold  = "2"
  name               = "gke-${var.clustername}-public-ingress-check"
  project            = var.project

  tcp_health_check {
    port         = "9080"
    proxy_header = "NONE"
  }

  timeout_sec         = "5"
  unhealthy_threshold = "3"
}


######
# backend
######
resource "google_compute_backend_service" "gke-primary-bal-backend" {
  affinity_cookie_ttl_sec = "0"

  backend {
    balancing_mode               = "RATE"
    capacity_scaler              = "1"
    group                        = data.google_compute_instance_group.gke-primary-default-pool.self_link
    max_connections              = "0"
    max_connections_per_endpoint = "0"
    max_connections_per_instance = "0"
    max_rate                     = "0"
    max_rate_per_endpoint        = "0"
    max_rate_per_instance        = "10000"
    max_utilization              = "0"
  }

  connection_draining_timeout_sec = "300"
  enable_cdn                      = "false"
  health_checks                   = [google_compute_health_check.gke-primary-public-ingress-check.self_link]
  load_balancing_scheme           = "EXTERNAL"

  log_config {
    enable      = "true"
    sample_rate = "1"
  }

  name             = "gke-${var.clustername}-bal-backend"
  port_name        = google_compute_instance_group_named_port.gke-primary-http-ingress-public-port.name
  project          = var.project
  protocol         = "HTTP"
  session_affinity = "NONE"
  timeout_sec      = "30"
}


######
# Mapeo de URL's (en este caso todo al mismo backend)
######
resource "google_compute_url_map" "gke-primary-l7-bal" {
  default_service = google_compute_backend_service.gke-primary-bal-backend.self_link
  name            = "gke-${var.clustername}-l7-bal"
  project         = var.project
}


# ######
# # Los proxys http y https
# # 
# ######
# resource "google_compute_target_https_proxy" "gke-primary-l7-bal-target-https-proxy-1" {
#   name             = "gke-${var.clustername}-l7-bal-target-https-proxy-1"
#   project          = var.project
#   quic_override    = "NONE"
#   ssl_certificates = [
#     data.google_compute_ssl_certificate.wildcard-vocento-com.self_link,
#     ]
#   url_map          = google_compute_url_map.gke-primary-l7-bal.self_link
#}

resource "google_compute_target_http_proxy" "gke-primary-l7-bal-target-http-proxy" {
  name    = "gke-${var.clustername}-l7-bal-target-http-proxy"
  project = var.project
  url_map = google_compute_url_map.gke-primary-l7-bal.self_link
}



######
# Los certificados para el proxy https
# Etos se instalan desde otro terraform ya que estos certs son comunes a cualquier proyecto
######
# data "google_compute_ssl_certificate" "wildcard-diariovasco-com" { name = "wildcard-diariovasco-com-2021" }


######
# IP Publica (reservamos una IP publica)
######
resource "google_compute_global_address" "gke-primary-l7-ip-1" {
  name = "gke-${var.clustername}-l7-ip-1"
}

# resource "google_compute_global_address" "gke-primary-l7-ip-2" {
#   name = "gke-${var.clustername}-l7-ip-2"
# }

######
# Las reglas de entrada (asociamos IP publia a proxy, tanto http como https)
######

# Los http
resource "google_compute_global_forwarding_rule" "gke-primary-l7-bal-frontend-http-1" {
  ip_address            = google_compute_global_address.gke-primary-l7-ip-1.address
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL"
  name                  = "gke-${var.clustername}-l7-bal-frontend-http-1"
  port_range            = "80-80"
  project               = var.project
  target                = google_compute_target_http_proxy.gke-primary-l7-bal-target-http-proxy.self_link
}

# # Los https
# resource "google_compute_global_forwarding_rule" "gke-primary-l7-bal-frontend-https-1" {
#   ip_address            = google_compute_global_address.gke-primary-l7-ip-1.address
#   ip_protocol           = "TCP"
#   load_balancing_scheme = "EXTERNAL"
#   name                  = "gke-${var.clustername}-l7-bal-frontend-https-1"
#   port_range            = "443-443"
#   project               = var.project
#   target                = google_compute_target_https_proxy.gke-primary-l7-bal-target-https-proxy-1.self_link
# }

