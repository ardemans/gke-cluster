# Ingress
resource "helm_release" "ingress" {
  depends_on        = [google_container_node_pool.primary_default-pool]
  name              = "ingress-nginx"
  repository        = "https://kubernetes.github.io/ingress-nginx"
  chart             = "ingress-nginx"
  version           = "3.22.0"
  namespace         = "ingress-nginx"
  create_namespace  = "true"
  values            = [file("${path.module}/helm/ingress-nginx-private.yaml")]
}

######
# Check de servidor
####
resource "google_compute_health_check" "gke-primary-private-ingress-check" {
  check_interval_sec = "10"
  healthy_threshold  = "2"
  name               = "gke-${var.clustername}-private-ingress-check"
  project            = var.project

  tcp_health_check {
    port         = "80"
    proxy_header = "NONE"
  }

  timeout_sec         = "5"
  unhealthy_threshold = "3"
}


# Backend para balanceo interno
resource "google_compute_region_backend_service" "gke-primary-bal" {
  affinity_cookie_ttl_sec = "0"

  backend {
    balancing_mode               = "CONNECTION"
    capacity_scaler              = "0"
    failover                     = "false"
    group                        = data.google_compute_instance_group.gke-primary-default-pool.self_link
    max_connections              = "0"
    max_connections_per_endpoint = "0"
    max_connections_per_instance = "0"
    max_rate                     = "0"
    max_rate_per_endpoint        = "0"
    max_rate_per_instance        = "0"
    max_utilization              = "0"
  }

  connection_draining_timeout_sec = "300"
  enable_cdn                      = "false"
  health_checks                   = [google_compute_health_check.gke-primary-private-ingress-check.self_link]
  load_balancing_scheme           = "INTERNAL"
  name                            = "gke-${var.clustername}-bal"
  project                         = var.project
  protocol                        = "TCP"
  region                          = var.region
  session_affinity                = "NONE"
  timeout_sec                     = "30"
}

# Forward rule
resource "google_compute_forwarding_rule" "gke-primary-bal-forwarding-rule" {
  all_ports              = "false"
  allow_global_access    = "false"
  backend_service        = google_compute_region_backend_service.gke-primary-bal.self_link
  ip_address             = var.bal-ip
  ip_protocol            = "TCP"
  is_mirroring_collector = "false"
  load_balancing_scheme  = "INTERNAL"
  name                   = "gke-${var.clustername}-bal-forwarding-rule"
  network                = "https://www.googleapis.com/compute/v1/projects/${var.networkProject}/global/networks/${var.network}"
  network_tier           = "PREMIUM"
  ports                  = ["80", "443"]
  project                = var.project
  region                 = var.region
  subnetwork             = "https://www.googleapis.com/compute/v1/projects/${var.networkProject}/regions/${var.region}/subnetworks/${var.bal-subnetwork}"
}


resource "google_dns_record_set" "gke-primary-bal-dns" {
  # provider = "google-beta"
  project      = var.networkProject
  managed_zone = var.dns_managed_zone
  name         = var.bal-dns-name
  type         = "A"
  rrdatas      = [var.bal-ip]
  ttl          = 300
}