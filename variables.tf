variable "project" {
}

variable "region" {
}

variable "zone" {
}

variable "bal-ip" {
}

variable "bal-dns-name" {
}

variable "bal-subnetwork" {
}

variable "clustername" {
}

variable "cluster_secondary_range_name" {
}

variable "services_secondary_range_name" {
}

variable "networkProject" {
}

variable "network" {
}

variable "subnetwork" {
}

variable "master_ipv4_cidr_block" {
}

variable "dns_managed_zone" {
}